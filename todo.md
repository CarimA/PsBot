MUST
====
- ~~implement room join/leave command into core.admin~~
- implement LCPL thingy

For ADV LC Room
---------------
- auto-invite ADV LC regulars on room join
- send an invite to all new users; ensure they don't receive another one later.
- ~~auto create tour every 30 minutes (post notification in chat too)~~
- auto cancel tour if not enough users join
- implement core.tournament
    - auto-apply autostart timer on tour creation
	- auto-apply autodq timer on tour start
	- dq anybody using illegal pokemon/levels

SHOULD
======
- implement hyperlink title page parsing
- implement core.mod
	- auto-mute spammers
	- auto-mute shouters
	- auto-mute stretchers
	- auto-ban blacklisted users
	- keep track of moderator actions
		- implement points system

COULD
=====
- implement .uptime into core
- implement trivia
	- implement team trivia
- implement hangman
- implement anagram game
- implement rock/paper/scissors game
- implement werewolf game
- implement kunc
- implement SHOWDOWN plays SHOWDOWN