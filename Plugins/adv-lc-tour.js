var timeout = pluginConfig['adv-lc-tour'].timeout;

module.exports.init = function(room, roomtype) {
    if (room !== 'groupchat-aerow-advlittlecup')
        return;
    
    say('groupchat-aerow-advlittlecup', '/announce A new tournament will automatically start __' + timeout + ' minutes__ from now!');
    startTimer();
}

module.exports.tournamentForceEnd = function(room) {
    if (room !== 'groupchat-aerow-advlittlecup')
        return;
    
    say('groupchat-aerow-advlittlecup', '/announce Sorry that this had to end prematurely, but a new tournament will automatically start __' + timeout + ' minutes__ from now!');
    startTimer();
}

module.exports.tournamentEnd = function(room) {
    if (room !== 'groupchat-aerow-advlittlecup')
        return;
    
    say('groupchat-aerow-advlittlecup', '/announce **Good games!** A new tournament will automatically start __' + timeout + ' minutes__ from now!');
    startTimer();
}

module.exports.tournamentCreate = function(room) {
    if (room !== 'groupchat-aerow-advlittlecup')
        return;
    
    // todo: this needs to go in tournamentStart
    //say('groupchat-aerow-advlittlecup', '/announce **Have fun!**');
}

function startTimer() {
    setTimeout(function() { startTournament() }, (timeout * 60 * 1000));
}

function startTournament() {
    say('groupchat-aerow-advlittlecup', '/tour new gen3ou, elimination, 16, 2');
    say('groupchat-aerow-advlittlecup', '/announce Banned Pokémon: **Scyther**, **Zigzagoon**, **Omanyte**, **Meditite**, **Chansey**, **Wynaut**. Banned Movesets: **Agility + Baton Pass**.')
    say('groupchat-aerow-advlittlecup', '/announce Please ensure that all Pokémon on your teams are **Level 5** and ADV LC eligible, I will __automatically disqualify__ anybody that has illegal Pokémon!');
}




