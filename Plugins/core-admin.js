module.exports.chat = function(room, user, timestamp, message) {
    // seeing as every command in here will require some whitelisting or immense permission...
    if (isAuthenticated('~', user)) {
        if (evaluate = commandify([ "eval", "evaluate", "js" ], message)) {
            try {
                say(room, 'Result: ' + JSON.stringify(eval(evaluate.join(','))));
            }
            catch (exception) {
                say(room, 'Result: Exception [' + exception.name + ': ' + exception.message + ']');
            }
        }
        
        if (shutdown = commandify([ "shutdown", "kill" ], message)) {
            process.exit(-1);
        }
        
        if (join = commandify("join", message)) {
            if (join[0]) {
                log.info('Joining ' + join[0]);
                say(room, '/join ' + join[0]);
            }
            else {
                say(room, 'Usage: /join {room}.');
            }
        }
        
        if (leave = commandify("leave", message)) {
            if (leave[0]) {
                log.info('Leaving ' + leave[0]);
                say(room, '/leave ' + leave[0]);
            }
            else {
                say(room, 'Usage: /leave {room}.');
            }
        }
    }
}