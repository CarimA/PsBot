var fs = require('fs');
var colors = require('colors');

module.exports.chat = function(room, user, timestamp, message) {
    if (about = commandify("about", message)) {
        say(room, 'I am CheirBot 5, a smarter bot made in Node JS. I am plugin-based, allowing users full freedom over any aspect of my functionality.');
    }
    
    if (git = commandify([ "git", "source", "sourcecode" ], message)) {
        say(room, "GitLab repository: https://gitlab.com/CarimA/PsBot");
    }
    
    if (suggestion = commandify([ "suggestion", "suggest", "bugreport", "idea" ], message)) {
        fs.appendFile('reports.txt', 'From ' + user.username + ': ' + suggestion.join(', ') + '\r\n', function (err) {
            whisper(user, '**Thank you for your suggestion/bug report.** Please do continue to suggest ideas or report problems.');
            log.info('Received a suggestion/bug report from ' + user.username.yellow + '.');
        });
    } 
    
    if (todo = commandify("todo", message)) {
        say(room, "Todo List: https://gitlab.com/CarimA/PsBot/blob/master/todo.md");
    }
}