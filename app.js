// load packages
var requireDir = require('require-dir');
var colors = require('colors');
var fs = require('fs');
var url = require('url');
var http = require('http');
var https = require('https');
var decache = require('decache');
var onFileChange = require("on-file-change");
var websocketClient = require('websocket').client;

global.enteredRooms = [];
global.connection = null;

// load all JS files within /Plugins.
var plugins = {};
//var plugins = requireDir('./Plugins');

// global logging functions
global.log = function(text) {
    console.log(text);
}

global.log.info = function(text) {
    console.log('[INFO]\t\t'.cyan + text);
}

global.log.debug = function(text) {
    //console.log('[DEBUG]\t\t'.magenta + text);
}

global.log.success = function(text) {
    console.log('[SUCCESS]\t'.green + text); 
}

global.log.error = function(text) {
    console.log('[ERROR]\t\t'.red + text);
}

global.log.warning = function(text) {
    console.log('[WARNING]\t'.yellow + text);
}

// global laziness functions
global.commandify = function(command, data) {
    // check if a range
    if (Array.isArray(command)) {
        for (var i = 0; i < command.length; i++) {
            var output = commandify(command[i], data);
            if (output)
                return output;
        }
        return;
    }
    
    if (data.substr(0, config.commandChar.length) !== config.commandChar)
        return;
    
    data = data.substr(config.commandChar.length);
    
    var cmd = data.split(' ')[0];
    if (cmd.toLowerCase() !== command.toLowerCase())
        return;
    
    data = data.substr(cmd.length).trim();
    
    var temp = data.split(',');
    var output = [];
    while (temp.length > 0)
        output.push(temp.shift().trim());
        
    return output;
}

global.connected = function() {
    return connection.connected;
}

global.sanitise = function(text) {
	return text.toLowerCase().replace(/[^a-z0-9]/g, '');
}

global.getUserData = function(user) {
    return {
        rank: user.charAt(0),
        username: user.trim(),
        sanitisedUsername: sanitise(user.substr(1))
    };
}

global.isAuthenticated = function(requiredRank, user) {
    if (config.whitelist.indexOf(user.sanitisedUsername) > -1)
        return true;
    
    var groups = {
    	' ': 0,
        '+': 1,
        '%': 2,
        '@': 3,
        '#': 4,
        '&': 5,
        '~': 6
    }
    
    if (groups[user] >= groups[requiredRank])
        return true;
    
    return false;
}

// global messaging function
var messageQueue = [];
var dequeueTimeout;
var lastSentAt;
global.say = function(room, message) {
    // if we're not connected, the message can't send. Return false.
    if (!connected)
        return false;
    
    var now = Date.now();
    
    if (now < lastSentAt + getThrottleTime() - 5) {        
        messageQueue.push({
            room: room,
            message: message
        });
        if (!dequeueTimeout)
			dequeueTimeout = setTimeout(dequeue, now - lastSentAt + getThrottleTime());
            
        return false;
    }
    
    connection.send(room + '|' + message);
    lastSentAt = now;
	if (dequeueTimeout) {
		if (messageQueue.length) {
			dequeueTimeout = setTimeout(dequeue, getThrottleTime());
		} else {
			dequeueTimeout = null;
		}
	}
    
    return true;
}

global.whisper = function(user, message) {
    if (user.sanitisedUsername) {
        say(currentRoom, '/w ' + user.sanitisedUsername + ', ' + message);
    }
    else {
        say(currentRoom, '/w ' + user + ', ' + message);
    }
}

global.safeSay = function(requiredRank, room, user, message) {
    if (isAuthenticated(requiredRank, user)) {
        say(room, message);
    }
    else {
        whisper(user, message);
    }
}

function dequeue() {
    var item = messageQueue.shift();
    say(item.room, item.message);
}

function getThrottleTime() {
    return (config.messageTime * 1000);
}

function kill(text) {
    log.error(text);
    process.exit(-1);
}

console.log('');
console.log('welcome to'.yellow);
console.log("  ,ad8888ba,  88                     88            88888888ba                        8888888888   ".yellow);
console.log(" d8\"'    `\"8b 88                     \"\"            88      \"8b              ,d       88           ".yellow);
console.log("d8'           88                                   88      ,8P              88       88  ____     ".yellow);
console.log("88            88,dPPYba,   ,adPPYba, 88 8b,dPPYba, 88aaaaaa8P'  ,adPPYba, MM88MMM    88a8PPPP8b,  ".yellow);
console.log("88            88P'    \"8a a8P_____88 88 88P'   \"Y8 88\"\"\"\"\"\"8b, a8\"     \"8a  88       PP\"     `8b  ".yellow);
console.log("Y8,           88       88 8PP\"\"\"\"\"\"\" 88 88         88      `8b 8b       d8  88                d8  ".yellow);
console.log(" Y8a.    .a8P 88       88 \"8b,   ,aa 88 88         88      a8P \"8a,   ,a8\"  88,      Y8a     a8P  ".yellow);
console.log("  `\"Y8888Y\"'  88       88  `\"Ybbd8\"' 88 88         88888888P\"   `\"YbbdP\"'   \"Y888     \"Y88888P\"   ".yellow);
console.log('');

try {
    // load config.
    var config = require('./config.json');
    global.pluginConfig = config.plugins;
}
catch (exception) {
    kill("config.json does not exist.");
}

log.info('Loading plugins...');
fs.readdirSync(__dirname + '/Plugins/').forEach(function(file) {
  if (file.match(/\.js$/) !== null) {
    var name = file.replace('.js', '');
    decache('./Plugins/' + file);
    plugins[name] = require('./Plugins/' + file);
    
    log.success('Loaded \'' + file.cyan + '\'.');
  }
});

if (config.enableWatchers) {
    log.info('Enabling file watchers for config.json and plugins.');
    fs.watchFile('./config.json', function(current, previous) {
        if (current.mtime <= previous.mtime)
            return;
        
        // config.json changed, reload it.
        try {
            delete require.cache[require.resolve('./config.json')];
            global.config = require('./config.json');
            log.success('Reloaded config.json.');
        }
        catch (exception) {
            log.error('Failed to reload config.json.');
        }
    });

    for (var p in plugins) {
        onFileChange('./Plugins/' + p + '.js', function() {
            // this particular plugin changed, reload it.
            try {
                //delete require.cache[require.resolve('./Plugins/' + filename)]
                decache('./Plugins/' + p + '.js');
                plugins[p] = require('./Plugins/' + p + '.js');
                log.success('Reloaded plugin \'' + p.cyan + '.js'.cyan + '\'.');
            }
            catch (exception) {
                log.error('Failed to reload plugin \'' +  p.cyan + '.js'.cyan + '\'.');
            }
        });
    }
}

log.info('Connecting to server...');

function connect(retry) {
    if (retry) 
        log.info('Retrying...');
    
    var ws = new websocketClient();
    
    ws.on('connectFailed', function(error) {
        log.error('Could not connect to server.');
        log.info('Retrying in 20 seconds...');
        
        setTimeout(connect(true), 20000);
    });
    
    ws.on('connect', function(con) {
        connection = con;
        log.success('Connected to server!');
        
        con.on('error', function(error) {
            log.error('Connection error: ' + error.stack);
        });
        
        con.on('close', function(code, reason) {
            log.warning('Connection was closed; ' + reason + ' (' + code + ').');
            log.info('Retrying in 20 seconds...');

            setTimeout(connect(true), 20000);            
        });
        
        con.on('message', function(response) {
            if (response.type !== 'utf8')
                return;
                
            parseRaw(response.utf8Data);
        });
    });
    
    ws.connect(config.server);
}

connect(false);

var currentRoom;
function parseRaw(data) {
    if (!data)
        return;
    
    if (data.indexOf('\n') < 0) {
        parse(data);
        return;
    }
    
    var spl = data.split('\n');
    if (spl[0].charAt(0) === '>') {
        currentRoom = spl.shift().substr(1);
        if (currentRoom === '')
            currentRoom = 'lobby';
    }
    
    for (var i = 0; i < spl.length; i++) {
        parse(spl[i]);
    }
}

function parse(data) {
    log.debug(data); 
    var message = data.split('|');
    
    switch (message[1]) {
            case 'challstr':
				log.info('Received Challenge String, logging in...');
				var id = message[2];
				var str = message[3];
        
                var actionUrl = url.parse('https://play.pokemonshowdown.com/action.php');            
            
				var requestOptions = {
					hostname: actionUrl.hostname,
					port: actionUrl.port,
					path: actionUrl.pathname,
					agent: false
				};

				var data;
				if (!config.password) {
					requestOptions.method = 'GET';
					requestOptions.path += '?act=getassertion&userid=' + sanitise(config.username) + '&challengekeyid=' + id + '&challenge=' + str;
				} else {
					requestOptions.method = 'POST';
					data = 'act=login&name=' + config.username + '&pass=' + config.password + '&challengekeyid=' + id + '&challenge=' + str;
					requestOptions.headers = {
						'Content-Type': 'application/x-www-form-urlencoded',
						'Content-Length': data.length
					};
				}

				var req = https.request(requestOptions, function (res) {
					res.setEncoding('utf8');
					var data = '';
					res.on('data', function (chunk) {
						data += chunk;
					});
					res.on('end', function () {
						if (data === ';') {
							kill('failed to log in; nick is registered - invalid or no password given');
						}
                        
						if (data.length < 50) {
							kill('failed to log in: ' + data);
						}

						if (data.indexOf('heavy load') !== -1) {
							log.error('the login server is under heavy load; trying again in one minute');
							setTimeout(function () {
								this.message(message);
							}.bind(this), 60 * 1000);
							return;
						}

						if (data.substr(0, 16) === '<!DOCTYPE html>') {
							log.error('Connection error 522; trying agian in one minute');
							setTimeout(function () {
								this.message(message);
							}.bind(this), 60 * 1000);
							return;
						}

						try {
							data = JSON.parse(data.substr(1));
							if (data.actionsuccess) {
								data = data.assertion;
							} else {
								kill('could not log in; action was not successful: ' + JSON.stringify(data));
							}
						} catch (e) {}
						say('', '/trn ' + config.username + ',0,' + data);
					}.bind(this));
				}.bind(this));

				req.on('error', function (err) {
					log.error('login error: ' + err.stack);
				});

				if (data) req.write(data);
				req.end();
				break;
            
            case 'updateuser':
				if (message[2] !== config.username) return;

				if (message[3] !== '1') {
					kill('failed to log in, still guest');
				}

				log.success('Logged in as \'' + message[2].yellow + '\'.');
				say('', '/blockchallenges');

                for (var i = 0; i < config.rooms.length; i++) {
                    say('', '/join ' + config.rooms[i]);
                }
        
                break;
            
            case 'raw': 
            
                if (message[2].indexOf('infobox-roomintro')) {
                    // room is now entered.
                    enteredRooms.pushUnique(currentRoom);
                    log.info('Entered ' + currentRoom);
                }
            
                // todo: remove from entered room if leaving room
            
                break;

            case 'init': 
                for (var p in plugins) {
                    if (plugins[p].init) {
                        try {
                            plugins[p].init(currentRoom, message[2]);
                        }
                        catch (exception) {
                            say(currentRoom, 'An exception occured in plugin \'' + p + '.js\' - ' + exception.name + ': ' + exception.message + '.');
                            log.error(exception.stack);
                        }
                    }
                }
                break;
            
            case 'c:':      
                if (!enteredRooms.contains(currentRoom))
                    return;
                    
                if (sanitise(message[3]) === sanitise(config.username))
                    return;
            
                for (var p in plugins) {
                    if (plugins[p].chat) {
                        try {
                            plugins[p].chat(currentRoom, getUserData(message[3]), message[2], message.slice(4).join('|'));
                        }
                        catch (exception) {
                            say(currentRoom, 'An exception occured in plugin \'' + p + '.js\' - ' + exception.name + ': ' + exception.message + '.');
                            log.error(exception.stack);
                        }
                    }
                }
                break;
            
            case 'tournament': 
                if (!enteredRooms.contains(currentRoom))
                    return;
            
                switch(message[2]) {
                    case 'create':
                        for (var p in plugins) {
                            if (plugins[p].tournamentCreate) {
                                try {
                                    plugins[p].tournamentCreate(currentRoom);
                                }
                                catch (exception) {
                                    say(currentRoom, 'An exception occured in plugin \'' + p + '.js\' - ' + exception.name + ': ' + exception.message + '.');
                                    log.error(exception.stack);
                                }
                            }
                        }        
                        break;
                        
                    case 'forceend':
                        for (var p in plugins) {
                            if (plugins[p].tournamentForceEnd) {
                                try {
                                    plugins[p].tournamentForceEnd(currentRoom);
                                }
                                catch (exception) {
                                    say(currentRoom, 'An exception occured in plugin \'' + p + '.js\' - ' + exception.name + ': ' + exception.message + '.');
                                    log.error(exception.stack);
                                }
                            }
                        }                        
                        break;
                        
                    case 'end':
                        for (var p in plugins) {
                            if (plugins[p].tournamentEnd) {
                                try {
                                    plugins[p].tournamentEnd(currentRoom);
                                }
                                catch (exception) {
                                    say(currentRoom, 'An exception occured in plugin \'' + p + '.js\' - ' + exception.name + ': ' + exception.message + '.');
                                    log.error(exception.stack);
                                }
                            }      
                        }
                        break;
                }
            
                break;

    }
}

Array.prototype.pushUnique = function (item){
    if(this.indexOf(item) == -1) {
    //if(jQuery.inArray(item, this) == -1) {
        this.push(item);
        return true;
    }
    return false;
}

Array.prototype.contains = function (item) {
    if (this.indexOf(item) == -1) {
        return false;
    }
    return true;
}