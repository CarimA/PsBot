PsBot
=====

A new chat bot for **[Pokémon Showdown](http://play.pokemonshowdown.com/)**. Unlike [Pokémon-Showdown-Bot](https://github.com/TalkTakesTime/Pokemon-Showdown-Bot), this bot provides a bigger focus on making it much easier to extend by allowing reloadable plugins that allow for better design: instead of putting every single command in a single file, you can split logic into their own script and have each script handle their own chat events, users joining/leaving, tournaments starting, etc. 

Out of the box, the plugins **Core** and **Core.Admin** are included. In future, **Core.Tournaments**, **Core.Moderation** and several games will be included.

Scripting
=========

On top of being able to write whatever using Javascript/NodeJS, PsBot provides a range of helpful methods that take care of the logic for you that make it easy to create commands as you know them.

Logging Methods
---------------

- **log.info(text)** - writes to the console, prepending [INFO] in blue.
- **log.debug(text)** - writes to the console, prepending [DEBUG] in purple. Silent by default.
- **log.success(text)** - writes the console, prepending [SUCCESS] in green.
- **log.warning(text)** - writes to the console, prepending [WARNING] in yellow.
- **log.error(text)** - writes to the console, prepending [ERROR] in red.

Chatting Methods
----------------

- **say(room, message)** - sends a message to the specified room. A queue is implemented to prevent the bot from bypassing throttle limits.
- **whisper(user, message)** - sends a private message to the specified user. 
- **safeSay(requiredRank, room, user, message)** - If the user's rank is equal to or greater than the required rank, the message will be sent to the specified room, otherwise, the user will receive a PM. 

General Methods
---------------

- **commandify(command, data)** - checks if the input data is a potential command. command can be an array allowing for alternate commands. Returns an array of command parameters if it is an applicable command.
Example use: `if (info = commandify('info', message)) ' do something with info`. 
- **connected()** - returns a boolean indicating if the bot is connected to a Pokémon Showdown server.
- **sanitise(text)** - strips punctuation and spaces from the given text and makes it lower case (for example, "Cheir Test" becomes "cheirtest").
- **getUserData(name)** - returns a user object containing a rank, their display name and their sanitised name. (for example, " Cheir Test" becomes { rank: ' ', username: 'Cheir Test', sanitisedUsername: 'cheirtest' } and "+Cheir" becomes { rank: '+', username: '+Cheir', sanitisedUsername: 'cheir' }).
- **isAuthenticated(requiredRank, user)** - returns a boolean indicating if the provided user is a rank equal or greater than the rank specified.

Credits
=======
- **Morfent/TalkTakesTime/Quinella** - copypasted challstr authentication.

License
=======
**ISC License (ISC) Copyright (c) 2016, Carim A.**

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.